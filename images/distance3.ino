#include <hcsr04.h>
#include <Servo.h>
#define trigPin 11
#define echoPin 12
Servo myservo;
int pos = 0; 

void setup() {
   myservo.attach(9);
  Serial.begin (9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

}

void loop() {
  long duration, distance;
  digitalWrite(trigPin, LOW);  // Added this line
  delayMicroseconds(2); // Added this line
  digitalWrite(trigPin, HIGH);
//  delayMicroseconds(1000); - Removed this line
  delayMicroseconds(10); // Added this line
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration/2) / 29.1;

    Serial.print(distance);
    Serial.println(" cm");
     delay(5);

  if (distance < 8) {
    for (pos = 0; pos <= 270; pos += 2) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(2);                       // waits 15ms for the servo to reach the position
  }
for (pos = 270; pos >= 0; pos -= 2) { // goes from 180 degrees to 0 degrees
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(2);                            // waits 15ms for the servo to reach the position
  }
    
  }
 
 
}
