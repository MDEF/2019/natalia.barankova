#include <hcsr04.h>

#define trigPin 11
#define echoPin 12
#define led 3
#define led1 5
#define led2 6




void setup() {
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(led, OUTPUT);
  pinMode(led1, OUTPUT);
    pinMode(led2, OUTPUT);


  Serial.begin(9600);
}

void loop() {
  long duration, distance,distance2,fadeValue;
  digitalWrite(trigPin, LOW);
  delayMicroseconds(20);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(20); 
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration/2) / 29.1;
  Serial.println (distance);
 
  if (distance < 7) { 
  fadeValue = map(distance, 0, 7, 0, 20);
analogWrite(led, fadeValue);
analogWrite(led1, fadeValue);
analogWrite(led2, fadeValue);

  
      delay(5);
}
else if (7 <distance< 200 ) { 
  fadeValue = map(distance, 10, 50, 50, 250);
analogWrite(led, fadeValue);
analogWrite(led1, fadeValue);
analogWrite(led2, fadeValue);


  
      delay(5);
}
  else {
    digitalWrite(led,LOW);
        digitalWrite(led1,LOW);
                digitalWrite(led2,LOW);


    delay(3);
  }
}
